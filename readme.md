# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

[group4.md](group4.md)

and place your solution in a fitting category in this file. It's best to create a branch while working on the exercise!

## Repositories anlegen

... put commands and explanation here

## Staging und Commits

... put commands and explanation here

## Logs und Repositories untersuchen

... put commands and explanation here

## Branches managen

    $ git checkout master 		legt den Pointer auf den master Branch
    $ git branch feature1 		erzeugt neuen Branch "feature1"
    $ git checkout -b work 		erzeugt neuen Branch "work" und legt den Pointer auf diesen Branch 
    $ git checkout work 		legt Pointer auf den Branch "work"
    $ git branch 				listet die lokalen Branches auf
    $ git branch -v 			listet die lokalen Branches und den letzten Commit auf
    $ git branch -a 			listet alle (remote und lokale) Branches auf
    $ git branch --merged 		zeigt alle Branches welche mit dem aktuellen gemerged sind
    $ git branch --no-merged 	zeigt alle Branches, welche mit dem aktuellen NICHT gemerged sind
    $ git branch -d work 		löscht lokal Branch "work"
    $ git branch -D work 		löscht lokal Branch "work" und ignoriert dabei Konflikte

## Merging von Branches

    $ git checkout master       Zum Hauptbranch wechseln
    $ git merge work            Änderungen von "work" in den "master" mergen
    $ git merge --abort         Den "merge"-Modus im Konfliktfall verlassen 
    $ git cat-file -p a3798b    Den Inhalt des Objekts mit dem Hash a3798b formattiert ausgeben lassen

Example 1:

    A - B (master)
         \
          C (work)
    
    $ git checkout master
    $ git merge work

Result and explanation here: merge von work in den master
    A - B  - D (master)
        \    /
          C (work)

Example 2:

    A - B - C - D (master)
         \      
          X - Y (work)
    
    $ git checkout master
    $ git merge work

Result and explanation here: merge von work in den master
    A - B - C - D - E(master)
        \           /
          X ------ Y (work)


Example 3:

    A - B - C - D (master)
         \
          X - Y (work)
    
    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

Result and explanation here: merge von master in den work, danach fast forward
    A - B - C -  D
         \       \
          X - Y - Z (master, work)